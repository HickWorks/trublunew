module.exports = {
  corePlugins: {
    preflight: false,
  },
  purge: {
    content: [
      './components/**/*.js',
      './lib/**/*.js',
      './modules/**/*.js',
      './pages/**/*.js',
    ],
    options: {
      safelist: [
        /^grid-cols-/,
        /^xs:grid-cols-/,
        /^sm:grid-cols-/,
        /^md:grid-cols-/,
        /^lg:grid-cols-/,
        /^xl:grid-cols-/,

        /^col-span-/,
        /^xs:col-span-/,
        /^sm:col-span-/,
        /^md:col-span-/,
        /^lg:col-span-/,
        /^xl:col-span-/,

        /^col-start-/,
        /^xs:col-start-/,
        /^sm:col-start-/,
        /^md:col-start-/,
        /^lg:col-start-/,
        /^xl:col-start-/,

        /^justify-self-/,
        /^xs:justify-self-/,
        /^sm:justify-self-/,
        /^md:justify-self-/,
        /^lg:justify-self-/,
        /^xl:justify-self-/,

        /^self-/,
        /^xs:self-/,
        /^sm:self-/,
        /^md:self-/,
        /^lg:self-/,
        /^xl:self-/,

        /^max-w-/,
        /^text-/,
      ],
    },
  },
  darkMode: false,
  theme: {
    fontFamily: {
      'sans': ['Mulish', 'Helvetica', 'Arial', 'sans-serif'],
      'special': ['Tomorrow', 'Helvetica', 'Arial', 'sans-serif'],
      'display': ['Black Ops One', 'Helvetica', 'Arial', 'sans-serif'],
      'body': ['Mulish', 'Helvetica', 'Arial', 'sans-serif'],
      'mono': ['Cascadia Mono', 'Segoe UI Mono', 'Roboto Mono', 'Oxygen Mono', 'Ubuntu Monospace', 'Source Code Pro', 'Fira Mono', 'Droid Sans Mono', 'Courier New'],
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: '#1c1c1c',
      white: '#FFFFFF',
      pageBG: 'var(--pageBG)',
      pageText: 'var(--pageText)',
      blue: {
        light: '#00C6FB',
        DEFAULT: '#005BEA',
      },
      indigo: {
        light: '#2A5298',
        DEFAULT: '#1E3C72',
        dark: '#172C52',
      },
      red: {
        light: '#FF0082',
        DEFAULT: '#FF1300',
      },
      yellow: {
        light: '#F6FF84',
        DEFAULT: '#EDFF00',
      },
      gray: {
        darkest: '#1C1C1C',
        dark: '#323232',
        DEFAULT: '#3f3f3f',
        light: '#e5e5e5',
        lightest: '#f9f9f9',
      }
    },
    screens: {
      xs: '480px',
      sm: '768px',
      md: '940px',
      lg: '1200px',
      xl: '1600px',
    },
    extend: {
      fontFamily: {
        inherit: 'inherit',
      },
      fontSize: {
        xxs: '.625rem',
      },
      zIndex: {
        '-1': '-10',
        50: 50,
        60: 60,
        70: 70,
        80: 80,
        90: 90,
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
